
/*
 * Profiling specific hardware.
 *
 * Right now this is just the profiling buffer, but
 * it will eventually also consist of the compressor
 * as well. There also needs to be a scheme for doing
 * the write back step.
 */

#ifndef PROF_H
#define PROF_H

// We use 8 KB buffers
#define PROF_BUFFER_SIZE 2048

// We have heap size of 16KB
// This is a subroutine so it is ok
#define MIN_HEAP_SIZE 4096

struct prof_buffer_t {
   int buffer[PROF_BUFFER_SIZE];
   int line_ptr;
   int bit_ptr;
};

struct queue_node_t {
   int id, cost;
};

// Code specifically ment to handle writing to the profiling buffer
void prof_buffer_init(struct prof_buffer_t * buffer);
void prof_buffer_clear(struct prof_buffer_t * buffer);
int prof_buffer_is_full(struct prof_buffer_t * buffer);
int prof_buffer_write(struct prof_buffer_t * buffer, int nBits, int val);
int prof_buffer_pull_byte(struct prof_buffer_t * buffer, int line, int byteId);

// Code specifically ment to handle the data compression step
void prof_compress_data(struct prof_buffer_t * ibuffer, struct prof_buffer_t * obuffer);
void prof_compress_fill_freq(struct prof_buffer_t * buffer, int * table);



#endif
