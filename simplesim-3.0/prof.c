
#include "prof.h"


void prof_buffer_init(struct prof_buffer_t * buffer)
{
   int i;
   for (i=0; i<PROF_BUFFER_SIZE; i++)
      buffer->buffer[i] = 0;
   buffer->line_ptr = 0;
   buffer->bit_ptr = 0;
}

void prof_buffer_clear(struct prof_buffer_t * buffer)
{
   // Just reinitialize the buffer
   prof_buffer_init(buffer);
}

int prof_buffer_is_full(struct prof_buffer_t * buffer)
{
   return (buffer->line_ptr >= PROF_BUFFER_SIZE);
}

// Write to the buffer, returns whether it was a success
// Only write at most 32 bits of data at a time
// We write the buffer in the little endian order
int prof_buffer_write(struct prof_buffer_t * buffer, int nBits, int val)
{
   if (nBits > 32)
      return 0;
  
   // We go a bit at a time for safety
   while (nBits-->0)
   {
      if (prof_buffer_is_full(buffer))
         return 0;
      if (val&1)
         buffer->buffer[buffer->line_ptr] += (~0)&(1<<buffer->bit_ptr);

      // Propagate the bits in the buffer
      buffer->bit_ptr++;
      if (buffer->bit_ptr == 32)
      {
         buffer->bit_ptr = 0;
         buffer->line_ptr++;
      }
      val = val >> 1;
   }

   // Success in writing all the data
   return 1;
}


// Pulls the specified byte from the profile buffer
int prof_buffer_pull_byte(struct prof_buffer_t * buffer, int line, int byteId)
{
   int shiftAmount = byteId*8;
   int mask = 0xff<<shiftAmount;
   return 0xff&((mask&(buffer->buffer[line]))>>shiftAmount);
}


void prof_compress_data(struct prof_buffer_t * ibuffer, struct prof_buffer_t * obuffer)
{
   int i, curByte;
   int tableSize = 1<<8;
   int freqTable[tableSize];

   // Encoding stuff
   int idTable[tableSize];
   int outCodeTable[tableSize];
   int outCodeBitLength[tableSize];
   
   // Compression stuff
   struct queue_node_t queue[tableSize*4];
   int front_ptr = 0;
   int back_ptr = 0;
   int id_cnt = 0;

   // Our table with the data that was compressed, this should happen when
   // the data is being placed in the buffer but we will do it here for
   // convenience and allow easy turning on and off of compression
   prof_compress_fill_freq(ibuffer, freqTable);

   // First go through the frequency table and place all the values in the queue
   for (i=0; i<tableSize; i++)
   {
      outCodeBitLength[i] = 0;
      outCodeTable[i] = 0;
      idTable[i] = -1;

      // Only encode this value if it is used
      if (freqTable[i] > 0) 
      {
         idTable[i] = id_cnt;
         queue[back_ptr].id = id_cnt++;
         queue[back_ptr].cost = freqTable[i];
         back_ptr++;
      }
   }

   // Run the algorithm to encode the data
   while (back_ptr-front_ptr > 1)
   {
      for (i=front_ptr+1; i<back_ptr; i++) {
         if (queue[i].cost < queue[front_ptr].cost) {
            struct queue_node_t tmp = queue[front_ptr];
            queue[front_ptr] = queue[i];
            queue[i] = tmp;
         }
      }
      int left = front_ptr++;
      for (i=front_ptr+1; i<back_ptr; i++) {
         if (queue[i].cost < queue[front_ptr].cost) {
            struct queue_node_t tmp = queue[front_ptr];
            queue[front_ptr] = queue[i];
            queue[i] = tmp;
         }
      }
      int right = front_ptr++;
      
      // Add a zero bit to everything on the left and remap the ids
      for (i=0; i<tableSize; i++)
      {
         if (idTable[i] == queue[left].id)
         {
            idTable[i] = id_cnt;
            outCodeBitLength[i]++;
         }
      }

      // Add a one bit to everything on the right and remap the ids
      for (i=0; i<tableSize; i++)
      {
         if (idTable[i] == queue[right].id)
         {
            idTable[i] = id_cnt;
            outCodeTable[i] |= 1<<outCodeBitLength[i];
            outCodeBitLength[i]++;
         }
      }

      // Add a new node to the back
      queue[back_ptr].id = id_cnt++;
      queue[back_ptr].cost = (queue[left].cost)+(queue[right].cost);
      back_ptr++;
   }

   //for (i=0; i<tableSize; i++) 
   //   printf("Code %d -> %x\n", i, outCodeTable[i]);
   //for (i=0; i<tableSize; i++) printf("Bit size %d -> %d\n", i, outCodeBitLength[i]);
   for (i=0; i<tableSize; i++)
      if (outCodeBitLength[i] > 32)
         printf("ERROR: Code for byte %x is of length %d\n", i, outCodeBitLength[i]);
   
   // Do the compression and write the result in the output buffer
   for (i=0; i<ibuffer->line_ptr; i++)
      for (curByte=0; curByte<4; curByte++)
      {
         int val = prof_buffer_pull_byte(ibuffer,i,curByte);
         prof_buffer_write(obuffer,outCodeBitLength[val],outCodeTable[val]);
      }
}


void prof_compress_fill_freq(struct prof_buffer_t * buffer, int * table)
{
   int u, curByte;
   int tableSize = 1<<8;
   for (u=0; u<tableSize; u++)
      table[u] = 0;

   //printf("FILLING\n");
   // Add all the values from the buffer to the frequency table
   // For now we will drop incomplete lines, just for convenience
   // this doesn't make us lose lines since we use full registers
   for (u=0; u<buffer->line_ptr; u++)
      for (curByte=0; curByte<4; curByte++)
         table[prof_buffer_pull_byte(buffer,u,curByte)]++;

   //for (u=0; u<tableSize; u++)
   //   printf("%d -> %d\n", u, table[u]);
}

